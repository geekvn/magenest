<?php

namespace Magenest\Movie\Controller\Adminhtml\Actor;

use Exception;
use Magenest\Movie\Model\ActorFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Save extends Action
{

    const ADMIN_RESOURCE = 'actor';

    protected $resultPageFactory;
    protected $tempFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ActorFactory $actorFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->tempFactory = $actorFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            try {
                $id = $data['actor_id'];

                $actor = $this->tempFactory->create()->load($id);

                $data = array_filter($data, function ($value) {
                    return $value !== '';
                });

                $actor->setData($data);
                $actor->save();
                $this->messageManager->addSuccess(__('Successfully saved the item.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                return $resultRedirect->setPath('*/*/index');
            } catch (Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData($data);
                return $resultRedirect->setPath('*/*/edit', ['id' => $actor->getId()]);
            }
        }

        return $resultRedirect->setPath('*/*/index');
    }
}