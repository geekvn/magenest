<?php

namespace Magenest\Movie\Model\ResourceModel\Actor;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Subscription Collection
 */
class Collection extends AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    protected $_idFieldName = 'actor_id';

    public function _construct()
    {
        $this->_init('Magenest\Movie\Model\Actor', 'Magenest\Movie\Model\ResourceModel\Actor');
    }

    protected function _initSelect()
    {
        parent::_initSelect();

        $this->getSelect()->joinLeft(
            ['two' => $this->getTable('magenest_movie_actor')],
            'main_table.movie_id = two.movie_id',
            ['actor_id']
        )->order('movie_id');
        return $this;
    }
}