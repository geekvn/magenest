<?php
namespace Magenest\Movie\Model\ResourceModel\Actor;

use Magento\Framework\DB\Select;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Zend_Db_Select;

/**
* Class Collection
* @package Magenest\Movie\Model\ResourceModel\Director
*/
class Data extends AbstractCollection
{
/**
* ID Field Name
*
* @var string
*/
protected $_idFieldName = 'actor_id';

/**
* Event prefix
*
* @var string
*/
protected $_eventPrefix = 'magenest_actor_collection';

/**
* Event object
*
* @var string
*/
protected $_eventObject = 'actor_collection';

/**
* Define resource model
*
* @return void
*/
protected function _construct()
{
$this->_init('Magenest\Movie\Model\Actor', 'Magenest\Movie\Model\ResourceModel\Actor');
}

/**
* Get SQL for get record count.
* Extra GROUP BY strip added.
*
* @return Select
*/
public function getSelectCountSql()
{
$countSelect = parent::getSelectCountSql();
$countSelect->reset(Zend_Db_Select::GROUP);

return $countSelect;
}

/**
* @param string $valueField
* @param string $labelField
* @param array $additional
*
* @return array
*/
protected function _toOptionArray($valueField = 'actor_id', $labelField = 'name', $additional = [])
{
return parent::_toOptionArray($valueField, $labelField, $additional);
}
}