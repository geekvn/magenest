<?php

namespace Magenest\Movie\Block;

use Magenest\Movie\Model\MovieFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class  Movie extends Template
{
    protected $_movieFactory;

    public function __construct(
        Context $context,
        MovieFactory $movieFactory,
        ResourceConnection $Resource
    )
    {
        $this->_movieFactory = $movieFactory;
        $this->_resource = $Resource;
        parent::__construct($context);
    }


    public function getMovieCollection()
    {
        $collection = $this->_movieFactory->create()->getCollection();
        return $collection;
    }
}